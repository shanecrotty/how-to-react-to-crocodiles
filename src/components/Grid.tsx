import React, { useState } from 'react';
import Container from './Container';
import '../styles/Grid.css';

interface GridProps {
    gridSize: number;
    searchQuery: string;
    searchType: 'title' | 'content';
    tagFilter: string[];
}

const Grid: React.FC<GridProps> = ({ gridSize, searchQuery, searchType, tagFilter }) => {
    const [expandedContainer, setExpandedContainer] = useState<{ row: number, column: number } | null>(null);

    const totalContainers = 128; // Define the total number of containers
    const containersData = Array.from({ length: totalContainers }, (_, index) => ({
        id: index,
        title: `Crocodile ${index + 1}`,
        content: `Content ${index + 1}`,
        tags: index % 2 === 0 ? ['Work', 'Urgent'] : ['Personal', 'Important'] // Example tags
    }));

    const handleContainerClick = (row: number, column: number) => {
        if (expandedContainer && expandedContainer.row === row && expandedContainer.column === column) {
            setExpandedContainer(null); // Collapse if the same container is clicked again
        } else {
            setExpandedContainer({ row, column });
        }
    };

    const filteredContainers = containersData.filter(container => {
        const query = searchQuery.toLowerCase();
        const matchesSearch = searchType === 'title'
            ? container.title.toLowerCase().includes(query)
            : container.content.toLowerCase().includes(query) || container.title.toLowerCase().includes(query);

        const matchesTags = tagFilter.length === 0 || tagFilter.some(tag => container.tags.includes(tag));

        return matchesSearch && matchesTags;
    });

    const renderGrid = () => {
        const containers = [];
        const rows = Math.ceil(filteredContainers.length / gridSize);
        for (let row = 0; row < rows; row++) {
            for (let column = 0; column < gridSize; column++) {
                const index = row * gridSize + column;
                const containerData = filteredContainers[index];
                if (containerData) {
                    const isExpanded = expandedContainer !== null && expandedContainer.row === row && expandedContainer.column === column;
                    containers.push(
                        <Container
                            key={`${row}-${column}`}
                            isExpanded={isExpanded}
                            title={containerData.title}
                            content={containerData.content}
                            tags={containerData.tags}
                            gridSize={gridSize}
                            onClick={() => handleContainerClick(row, column)}
                        />
                    );
                }
            }
        }
        return containers;
    };

    return (
        <div className="grid-container">
            <div className={`grid grid-${gridSize}x${gridSize}`}>
                {renderGrid()}
            </div>
        </div>
    );
};

export default Grid;
