import React, {useState} from 'react';
import {
    Select,
    MenuItem,
    Checkbox,
    ListItemText,
    FormControl,
    InputLabel,
    OutlinedInput,
    RadioGroup,
    FormControlLabel,
    Radio,
    SelectChangeEvent,
    TextField,
    Typography,
    Box, MenuProps, FormLabel,
} from '@mui/material';
import '../styles/Header.css';

interface HeaderProps {
    onGridSizeChange: (value: number) => void;
    onSearch: (query: string) => void;
    onSearchTypeChange: (type: 'title' | 'content') => void;
    onTagFilterChange: (selectedTags: string[]) => void;
    availableTags: string[];
}

const Header: React.FC<HeaderProps> = ({
                                           onGridSizeChange,
                                           onSearch,
                                           onSearchTypeChange,
                                           onTagFilterChange,
                                           availableTags,
                                       }) => {
    const [searchType, setSearchType] = useState<'title' | 'content'>('title');
    const [selectedTags, setSelectedTags] = useState<string[]>([]);
    const [gridSize, setGridSize] = useState<string>('4');

    const handleGridSizeChange = (event: SelectChangeEvent<string>) => {
        const value = event.target.value as string;
        setGridSize(value);
        onGridSizeChange(Number(value));
    };

    const handleSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        onSearch(event.target.value);
    };

    const handleSearchTypeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const type = event.target.value as 'title' | 'content';
        setSearchType(type);
        onSearchTypeChange(type);
    };

    const handleTagChange = (event: SelectChangeEvent<string[]>) => {
        const value = event.target.value as string[];
        setSelectedTags(value);
        onTagFilterChange(value);
    };

    const customMenuProps: Partial<MenuProps> = {
        PaperProps: {
            style: {
                backgroundColor: '#263571',
                color: 'white',
            },
        },
    };

    return (
        <div className="header-content">
            <Typography variant="h4" component="h1" align="center"
                        sx={{fontFamily: 'BasierSquare-Medium, Arial, serif', color: '#FEDA14'}}>
                How to React to Crocodiles
            </Typography>
            <Box display="flex" justifyContent="center" alignItems="center" flexWrap="wrap" mt={2}>
                <FormControl variant="outlined" margin="normal" style={{minWidth: 60}} className="custom-form-control">
                    <InputLabel>Size</InputLabel>
                    <Select
                        value={gridSize}
                        onChange={handleGridSizeChange}
                        input={<OutlinedInput label="Grid Size"/>}
                        MenuProps={customMenuProps}
                    >
                        <MenuItem value="4">4</MenuItem>
                        <MenuItem value="8">8</MenuItem>
                    </Select>
                </FormControl>
                <TextField
                    label="Search"
                    variant="outlined"
                    margin="normal"
                    onChange={handleSearchChange}
                    className="custom-form-control"
                    style={{marginLeft: 16, marginRight: 16}}
                />
                <FormControl component="fieldset" margin="normal" className="custom-form-control">
                    <FormLabel>By: </FormLabel>
                    <RadioGroup row aria-label="search-type" name="search-type" value={searchType}
                                onChange={handleSearchTypeChange}>
                        <FormControlLabel value="title" control={<Radio className="custom-radio-group"/>}
                                          label="Title"/>
                        <FormControlLabel value="content" control={<Radio className="custom-radio-group"/>}
                                          label="Content"/>
                    </RadioGroup>
                </FormControl>
                <FormControl variant="outlined" margin="normal" style={{minWidth: 140}} className="custom-form-control">
                    <InputLabel>Filter by Tags</InputLabel>
                    <Select
                        multiple
                        value={selectedTags}
                        onChange={handleTagChange}
                        input={<OutlinedInput label="Filter by Tags"/>}
                        MenuProps={customMenuProps}
                        renderValue={(selected) => (selected as string[]).join(', ')}
                    >
                        {availableTags.map(tag => (
                            <MenuItem key={tag} value={tag}>
                                <Checkbox checked={selectedTags.indexOf(tag) > -1}
                                          sx={{color: '#FEDA14', '&.Mui-checked': {color: '#FEDA14'}}}
                                          className="custom-checkbox"/>
                                <ListItemText primary={tag} className="custom-checkbox"/>
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </Box>
        </div>
    );
};

export default Header;
