import React from 'react';
import { Chip } from '@mui/material';
import '../styles/Container.css';

interface ContainerProps {
    isExpanded: boolean;
    title: string;
    content: string;
    tags: string[];
    gridSize: number;
    onClick: () => void;
}

const Container: React.FC<ContainerProps> = ({ isExpanded, title, content, tags, gridSize, onClick }) => {
    const containerClass = isExpanded ? 'container expanded' : 'container';
    const style = isExpanded
        ? { '--grid-span': gridSize, '--row-span': gridSize } as React.CSSProperties
        : {};

    return (
        <div className={containerClass} onClick={onClick} style={style}>
            {gridSize <= 8 ? <h3>{title}</h3> : <h4>{title}</h4>}
            <p>{content}</p>
            <div className="tags">
                {tags.map(tag => (
                    <Chip key={tag} label={tag} className="tag" />
                ))}
            </div>
        </div>
    );
};

export default Container;
