import React, { useState } from 'react';
import Grid from './components/Grid';
import Header from './components/Header';
import './styles/App.css';

const App: React.FC = () => {
    const [gridSize, setGridSize] = useState<number>(4);
    const [searchQuery, setSearchQuery] = useState<string>('');
    const [searchType, setSearchType] = useState<'title' | 'content'>('title');
    const [tagFilter, setTagFilter] = useState<string[]>([]);

    const handleGridSizeChange = (value: number) => {
        setGridSize(value);
    };

    const handleSearch = (query: string) => {
        setSearchQuery(query);
    };

    const handleSearchTypeChange = (type: 'title' | 'content') => {
        setSearchType(type);
    };

    const handleTagFilterChange = (selectedTags: string[]) => {
        setTagFilter(selectedTags);
    };

    const availableTags = ['Work', 'Personal', 'Important', 'Urgent'];

    return (
        <div className="App">
            <header className="header">
                <Header
                    onGridSizeChange={handleGridSizeChange}
                    onSearch={handleSearch}
                    onSearchTypeChange={handleSearchTypeChange}
                    onTagFilterChange={handleTagFilterChange}
                    availableTags={availableTags}
                />
            </header>
            <main className="main">
                <Grid gridSize={gridSize} searchQuery={searchQuery} searchType={searchType} tagFilter={tagFilter} />
            </main>
        </div>
    );
};

export default App;
